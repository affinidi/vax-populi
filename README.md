# Vax Populi #



### What is Vax Populi? ###

**Vax Populi** is a **Verifiable Credential** (**VC**) passport that verifies the fully vaccinated status of a holder against **COVID-19**. 